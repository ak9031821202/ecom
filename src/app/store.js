import { configureStore } from "@reduxjs/toolkit";
import productReducer from "../features/product/productListSlice";

const store = configureStore({
  reducer: { product: productReducer },
});

export default store;
