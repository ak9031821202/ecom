import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import { Provider, createStoreHook } from "react-redux";
import store from "./app/store"; // assuming you have a root reducer
import App from "./App";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);
