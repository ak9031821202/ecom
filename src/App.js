import "./App.css";
import Login from "./features/auth/components/Login";
import Signup from "./features/auth/components/Signup";
import {
  createBrowserRouter,
  RouterProvider,
  Route,
  Link,
} from "react-router-dom";
import Home from "./pages/Home";
import CartPage from "./pages/CartPage";
import Checkout from "./pages/Checkout";
import ProductDetailPage from "./pages/ProductDetailPage";

function App() {
  const router = createBrowserRouter([
    {
      path: "/",
      element: <Home />,
    },
    {
      path: "/login",
      element: <Login />,
    },
    {
      path: "/signup",
      element: <Signup />,
    },
    {
      path: "/cart",
      element: <CartPage></CartPage>,
    },
    {
      path: "/checkout",
      element: <Checkout></Checkout>,
    },
    {
      path: "/product-detail",
      element: <ProductDetailPage></ProductDetailPage>,
    },
  ]);

  return <RouterProvider router={router} />;
}

export default App;
